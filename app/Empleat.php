<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleat extends Model {

	protected $table = 'empleat';
	
	public function empresa()
    {
        return $this->belongsTo('Empresa');
    }
	
	public function rangempleat()
	{
		return $this->hasOne('App\Rangempleat','idrang');
	}
	
}
