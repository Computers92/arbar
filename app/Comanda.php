<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comanda extends Model {

	protected $table = 'comanda';
	
	
	public function usuari()
	{
		return $this->hasOne('App\User','id','idusuari');
	}
	
	public function idtaula()
	{
		return $this->hasOne('App\Taula','idtaula');
	}
	
	public function empleat()
	{
		return $this->hasOne('App\Empleat','id','idempleat');
	}
	
	public function preufinal()
	{
		return $this->hasOne('App\Preufinalcomanda', 'idcomanda');	
	}
	
	public function detall(){
		
		return $this->belongsTo('App\Detallcomanda');	
	}


	
}
