<?php namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Empleat;
use App\Empresa;
use App\Producte;
use App\Valoracio;
use App\Taula;
use App\User;
use App\Comanda;
use App\Detallcomanda;
use App\Tipusproducte;
use App\Preufinalcomanda;
use App\Estatdetallcomanda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;



class ArBarController extends Controller {

	
	
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$idusuari = Auth::user()->id;	
		
		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();
		
		$ultimsproductes = Producte::take(4)->orderby('created_at','asc')->get();
		
		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();
		$tipusproductes = Tipusproducte::all();
		
		$totalcomanda = 0;
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}
		
		return view('arbar.index', ['comanda' => $comanda, 
									'detallcomanda' => $detallcomanda,
									'totalcomanda' => $totalcomanda,
									'tipusproductes' => $tipusproductes,
									'ultimsproductes' => $ultimsproductes
								   ]);
		
	}
	
	public function prueba(){
		$detallcomanda = Comanda::where('idusuari','=',Auth::user()->id)->with('detall')->get();
		if(count($detallcomanda)>0){
			echo $detallcomanda;
			echo "Si";
		}else{
			echo $detallcomanda->delete();
			echo "NO";
			echo "Hay que borrar esto de aquí";
		}
	}
		
	
	public function demana($idtipus)
	{

		$idusuari = Auth::user()->id;

		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();

		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();

		$totalcomanda = 0;
		
		
		if($idtipus==0){
			$productes = Producte::all();
			$nomtipus = 'Tots els productes';
		} else {
			$productes = Producte::where('stock','!=',0)->where('idtipus','=',$idtipus)->get();
			$tipusproductes = Tipusproducte::all();

			$nt = Tipusproducte::find($idtipus);
			$nomtipus = $nt->nom;
		}
		
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}
		
		$tipusproductes = Tipusproducte::all();
		
		return view('arbar.items', ['comanda' => $comanda, 
									'detallcomanda' => $detallcomanda,
									'totalcomanda' => $totalcomanda,
									'productes' => $productes,
									'tipusproductes' => $tipusproductes,
									'nomtipus' => $nomtipus
								   ]);
	}

	public function borraElementComanda($id){
		
		$productedetall = Detallcomanda::find($id);
		$producte = Producte::find($productedetall->idproducte);
		$producte->stock = $producte->stock+1;
		$producte->save();
		
		$detall = Detallcomanda::destroy($id);
		
		return redirect('/');
	}
	
	public function afegeixElementComanda($idproducte){
		
		$idusuari = Auth::user()->id;
		$comandaActiva = Comanda::where('idusuari','=',$idusuari)->where('estat','=',0)->get();
		
		Detallcomanda::insert(
			array('idcomanda' => $comandaActiva[0]->id, 'idproducte' => $idproducte, 'idestat' => 1)
		);
		
		$prod = Producte::find($idproducte);
		$prod->stock = $prod->stock-1;
		$prod->save();
		
		return redirect('demana/'.$prod->idtipus.'');
		
	}
	
	public function confirmacomanda(){
	
		$idusuari = Auth::user()->id;
		$comandausuari = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();
		
		$comandaActiva = Detallcomanda::where('idestat','=',1)->where('idcomanda','=',$comandausuari[0]->id)->get();
		
		for($i=0; $i<sizeof($comandaActiva); $i++){
			$comandaActiva[$i]->idestat='2';
			$comandaActiva[$i]->save();
		}
		
		return redirect('/');
	}

	public function historicComandes(){
	
		$idusuari = Auth::user()->id;
		$comandes = Comanda::where('idusuari','=',$idusuari)->where('estat','=',1)->get();
		
		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();
		$detallscomandes = Detallcomanda::where('idestat','=',3)->get();

		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();
		
		$totalcomanda = 0;
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}
		
		$tipusproductes = Tipusproducte::all();
		
		return view('arbar.historiccomandes', [	'comandes' => $comandes,
											   	'comanda' => $comanda,
											   'detallcomanda' => $detallcomanda,
											   'totalcomanda' => $totalcomanda,
											   'tipusproductes' => $tipusproductes,
											   'detallscomandes' => $detallscomandes
											   
											  	
					]);
	}
	
	
	public function valoraProducte($idproducte){
	
		$idusuari = Auth::user()->id;
		
		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();
		
		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();
		$tipusproductes = Tipusproducte::all();

		$totalcomanda = 0;
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}

		$producte = Producte::find($idproducte);
		
		return view('arbar.valoraproducte', ['comanda' => $comanda, 
									'detallcomanda' => $detallcomanda,
									'totalcomanda' => $totalcomanda,
									'tipusproductes' => $tipusproductes,
									'producte' => $producte
								   ]);
		
		
	}
	
	public function ferValoracioProducte(){
		
		$idusuari = Auth::user()->id;
		$estrelles = \Request::input('estrelles');
		$comentari = \Request::input('comentari');
		$idproducte = \Request::input('idproducte');
		
		Valoracio::insert(
			array('idproducte' => $idproducte, 
				  'idusuari' => $idusuari,
				  'qualificacio' => $estrelles,
				  'comentari' => $comentari,
				  'llegida' => 0
				 )
		);
		
		return Redirect::back()->with('msg', 'Valoració guardada correctament');
	}
	
	
	public function valoracionsFetes(){
	
		$idusuari = Auth::user()->id;

		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();

		$valoracions = Valoracio::where('idusuari','=',$idusuari)->get();

		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();
		$tipusproductes = Tipusproducte::all();

		$totalcomanda = 0;
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}

		return view('arbar.valoracionsfetes', [	'comanda' => $comanda, 
												'detallcomanda' => $detallcomanda,
												'totalcomanda' => $totalcomanda,
												'tipusproductes' => $tipusproductes,
											    'valoracions' => $valoracions
								   ]);		
	}
	
	public function perfilUsuari(){
	
		$idusuari = Auth::user()->id;
		$usuari = User::find($idusuari);
		
		$comanda = Comanda::where('estat','=',0)->where('idusuari','=',$idusuari)->get();
		$detallcomanda = Detallcomanda::where('idcomanda','=',$comanda[0]->id)->where('idestat','=',1)->get();
		$tipusproductes = Tipusproducte::all();
		$comandesfetes = Comanda::where('idusuari','=',$idusuari)->count();
		$valoracionsfetes = Valoracio::where('idusuari','=',$idusuari)->count();
		
		$totalcomanda = 0;
		for($i = 0; $i<sizeof($detallcomanda); $i++){
			$totalcomanda = $detallcomanda[$i]->producte->preu + $totalcomanda;
		}
		
		return view('arbar.perfilusuari', ['comanda' => $comanda,
										   'detallcomanda' => $detallcomanda,
										   'totalcomanda' => $totalcomanda,
										   'tipusproductes' => $tipusproductes,
										   'usuari' => $usuari,
										   'comandesfetes' => $comandesfetes,
										   'valoracionsfetes' => $valoracionsfetes
										  ]);		
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
