<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Empleat;
use App\Empresa;
use App\Producte;
use App\Valoracio;
use App\Taula;
use App\User;
use App\Comanda;
use App\Detallcomanda;
use App\Preufinalcomanda;
use App\Estatdetallcomanda;
use App\Tipusproducte;
use Illuminate\Http\Request;

class AdminController extends Controller {

	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$empleats = Empleat::all();
		$empresa = Empresa::all();	
		$missatgespendents = Valoracio::where('llegida', '=', 0)->count();
		$tauleslliures = Taula::where('estat', '=', 0)->count();
		$comandespendents = Comanda::where('estat', '=', 0)->count();
		$productessensestock = Producte::where('stock', '=', 0)->count();
		
		return view('admin.admininici', ['empleats' => $empleats, 
										'empresa' => $empresa, 
										'missatgespendents' => $missatgespendents,
										'tauleslliures' => $tauleslliures,
										'comandespendents' => $comandespendents,
										'productessensestock' => $productessensestock
										
		]);
	}
	
	public function taules()
	{
		
		$taules = Taula::all();
		$tauleslliures = Taula::where('estat', '=', 0)->count();
		$empresa = Empresa::all();

			
		return view('admin.taules', ['taules' => $taules, 
									 'empresa' => $empresa, 
									 'tauleslliures' => $tauleslliures
		]);
	}
	
	public function updateTaula($t){
	
		$updatetaula = Taula::find($t);
		
		if($updatetaula->estat=='0'){
			
			$updatetaula->estat='1';
			$updatetaula->save();
		} else {
			
			$updatetaula->estat='0';
			$updatetaula->save();
		}
	
		$taules = Taula::all();
		$tauleslliures = Taula::where('estat', '=', 0)->count();
		$empresa = Empresa::all();
		
		return view('admin.taules', ['taules' => $taules, 
									 'empresa' => $empresa, 
									 'tauleslliures' => $tauleslliures
									]);
	}
	
	
	public function valoracions()
	{
		
		
		$valoracions = Valoracio::with('producte')->get();
		
		$valoracionsnollegides = Valoracio::where('llegida', '=', 0)->count();
		$empresa = Empresa::all();
	
		return view('admin.valoracions', ['valoracions' => $valoracions, 
									 		   'empresa' => $empresa, 
											   'valoracionsnollegides' => $valoracionsnollegides
									]);
	}

	
	public function updateValoracio($v)
	{
	
		$updatevaloracio = Valoracio::find($v);

		if($updatevaloracio->llegida=='0'){

			$updatevaloracio->llegida='1';
			$updatevaloracio->save();
		}
		
		$empresa = Empresa::all();
		$valoracio = Valoracio::with('producte')->where('id', '=', $v)->get();
		
		
		
		return view('admin.llegirvaloracio', ['valoracio' => $valoracio, 
										  'empresa' => $empresa, 
										 ]);
	
	
	}
	
	public function usuaris()
	{

		$empresa = Empresa::all();
		$numusuaris = User::all()->count();
		$usuaris = User::all();
		return view('admin.usuaris', ['empresa' => $empresa, 
									  'numusuaris' => $numusuaris,
									  'usuaris' => $usuaris
										]);


	}
	
	public function comandes()
	{

		$empresa = Empresa::all();
		$comandes = Comanda::where('estat','=',0)->get();
	
		$detallscomandes = Detallcomanda::where('idestat','!=', 1)->get();
		
		return view('admin.comandes', ['empresa' => $empresa, 
									   'comandes' => $comandes,
									   'detallscomandes' => $detallscomandes
									 ]);
	}
	
	public function updateEstatDetallComanda($c){
		
		$detallcomanda = Detallcomanda::find($c);

		if($detallcomanda->idestat=='2'){

			$detallcomanda->idestat='3';
			$detallcomanda->save();
		}
		
		
		$empresa = Empresa::all();
		$comandes = Comanda::where('estat','=',0)->get();
		$detallscomandes = Detallcomanda::all();
		
		return view('admin.comandes', ['empresa' => $empresa, 
									   'comandes' => $comandes,
									   'detallscomandes' => $detallscomandes
									  ]);
		
	}
	
	public function finalitzaComanda($c){
	
		$empresa = Empresa::all();
		$comanda = Comanda::find($c);
		$detallcomandes = Detallcomanda::where('idcomanda','=',$c)->get();
		
		for($i = 0; $i<sizeof($detallcomandes); $i++){
			if($detallcomandes[$i]->idestat!='3'){
				$detallcomandes[$i]->idestat='3';
				$detallcomandes[$i]->save();
			}
		}

		if($comanda->estat=='0'){
			$comanda->estat='1';
			$comanda->save();
		}
		
		$preuf = 0;
		
		for($i = 0; $i<sizeof($detallcomandes); $i++){
			$preuf = $detallcomandes[$i]->producte->preu + $preuf;
		}
		
		Preufinalcomanda::insert(
			array('idcomanda' => $comanda->id, 'preu' => $preuf)
		);
		

		return view('admin.comandesfinalitza', ['empresa' => $empresa,
												'comanda' => $comanda,
												'detallcomandes' => $detallcomandes,
												'preufinal' => $preuf
												
									  ]);
		
	}
	
	public function historic(){
		
		$empresa = Empresa::all();
		$comandes = Comanda::where('estat','=',1)->get();
		$totalcomandes = Comanda::where('estat','=',1)->count();

		return view('admin.historic', ['comandes' => $comandes, 
									   'totalcomandes' => $totalcomandes,
									   'empresa' => $empresa
									]);
	}
	
	public function productes(){
		
		$empresa = Empresa::all();
		$productes = Producte::orderBy('stock')->get();
		return view('admin.productes', ['productes' => $productes, 
									   'empresa' => $empresa
									  ]);
		
	}
	
	public function nouProducte(){

		$empresa = Empresa::all();
		$tipusproductes = Tipusproducte::lists('nom','id');
		
		return view('admin.nouproducte', ['tipusproductes' => $tipusproductes, 
										'empresa' => $empresa
									   ]);

	}
	
	public function crearNouProducte(){
			
		$nom = \Request::input('nom');
		$descripcio = \Request::input('descripcio');
		$tipus = \Request::input('tipus');
		$preu = \Request::input('preu');
		$stock = \Request::input('stock');
		
		$file = \Request::file('image');
		$fileName = $file->getClientOriginalName();
		$file->move(public_path().'/img/productes/', $fileName);
		
		$ruta = '/img/productes/'.$fileName;
		
		Producte::insert(
			array('idtipus' => $tipus, 
				  'nom' => $nom,
				  'descripcio' => $descripcio,
				  'preu' => $preu,
				  'img' => $ruta,
				  'stock' => $stock)
		);
		
		$empresa = Empresa::all();
		$productes = Producte::orderBy('stock')->get();
		return view('admin.productes', ['productes' => $productes, 
										'empresa' => $empresa
									   ]);
	}
	
	public function editaProducte($idprod){
		
		$empresa = Empresa::all();
		$producte = Producte::find($idprod);
		$tipusproductes = Tipusproducte::lists('nom','id');
	
		
		return view('admin.modificaproducte', ['producte' => $producte,
											   'empresa' => $empresa,
											   'tipusproductes' => $tipusproductes
											   ]);
		
	}
	
	public function modificaProducteExistent(){
		
		
		$nom = \Request::input('nom');
		$descripcio = \Request::input('descripcio');
		$idtipus = \Request::input('tipus');
		$preu = \Request::input('preu');
		$stock = \Request::input('stock');
		$idproducte = \Request::input('idproducte');

		$file = \Request::file('image');
		
		$prodactual = Producte::find($idproducte);
		
		if($prodactual->nom != $nom){
			$prodactual->nom = $nom;
			$prodactual->save();
		}
		
		if($prodactual->descripcio != $descripcio){
			$prodactual->descripcio = $descripcio;
			$prodactual->save();
		}
		
		if($prodactual->idtipus != $idtipus){
			$prodactual->idtipus = $idtipus;
			$prodactual->save();
		}
		
		if($prodactual->preu != $preu){
			$prodactual->preu = $preu;
			$prodactual->save();
		}
		
		if($prodactual->stock != $stock){
			$prodactual->stock = $stock;
			$prodactual->save();
		}
		
		if($file){

			$file = \Request::file('image');
			$fileName = $file->getClientOriginalName();
			$file->move(public_path().'/img/productes/', $fileName);

			$ruta = '/img/productes/'.$fileName;
			
			$prodactual->img = $ruta;
			
			$prodactual->save();
		}
		
		$empresa = Empresa::all();
		$productes = Producte::orderBy('stock')->get();
		
		
		
		return view('admin.productes', ['productes' => $productes, 
										'empresa' => $empresa
									   ]);
		
	}
	
	public function estadistiques(){
		$empresa = Empresa::all();
		
		$range = 30;
		$productes = Producte::orderBy('stock')->get();
		$stats = Tipusproducte::all();									
									
										
	
		return view('admin.estadistiques', ['productes' => $productes,
											'stats' => $stats,
											'empresa' => $empresa
									   ]);	
	}
		
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
