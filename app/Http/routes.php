<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ArBarController@index');
Route::get('about', 'ArBarController@about');
Route::get('borraElementComanda/{id}', 'ArBarController@borraElementComanda');

Route::post('crearNouProducte', 'AdminController@crearNouProducte');

Route::get('demana/{idtipus}', 'ArBarController@demana');
Route::get('confirmacomanda', 'ArBarController@confirmacomanda');

Route::get('historiccomandes', 'ArBarController@historicComandes');
Route::get('valoraproducte/{id}', 'ArBarController@valoraProducte');
Route::post('ferValoracioProducte', 'ArBarController@ferValoracioProducte');


Route::get('perfilusuari', 'ArBarController@perfilUsuari');


Route::get('valoracionsfetes', 'ArBarController@valoracionsFetes');


Route::get('afegeixElementComanda/{idproducte}', 'ArBarController@afegeixElementComanda');


Route::get('admin', 'AdminController@index');

Route::get('estadistiques', 'AdminController@estadistiques');

Route::get('taulesadmin', 'AdminController@taules');
Route::get('updatetable/{id}', 'AdminController@updateTaula'); 

Route::get('valoracionsadmin', 'AdminController@valoracions');
Route::get('updatevaloracio/{id}', 'AdminController@updateValoracio'); 

Route::get('usuarisadmin', 'AdminController@usuaris');
Route::get('productesadmin', 'AdminController@productes');
Route::get('nouproducte', 'AdminController@nouProducte');

Route::get('comandesadmin', 'AdminController@comandes');
Route::get('updateEstatDetallComanda/{id}', 'AdminController@updateEstatDetallComanda');
Route::get('finalitzaComanda/{id}', 'AdminController@finalitzaComanda');

Route::get('historicadmin', 'AdminController@historic');
Route::get('prueba', 'ArBarController@prueba');
Route::get('editaProducte/{id}', 'AdminController@editaProducte');
Route::post('modificaProducteExistent', 'AdminController@modificaProducteExistent');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
