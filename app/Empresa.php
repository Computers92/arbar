<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Empleat;
class Empresa extends Model {

	protected $table = 'empresa';
	
	public function empleats()
    {
        return $this->hasMany('App\Empleat','idempresa');
    }

	
}
