<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class usersrols extends Model {

	protected $table = 'usersrols';

	public function users()
	{
		return $this->belongsTo('App\Users', 'iduser', 'id');
	}


}
