<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Preufinalcomanda extends Model {

	protected $table = 'preufinalcomanda';

	public function preufinal()
	{
		return $this->hasOne('App\Comanda','idcomanda','id');
	}
}
