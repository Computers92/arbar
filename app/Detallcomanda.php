<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallcomanda extends Model {

	protected $table = 'detallcomanda';

	
	public function estat()
	{
		return $this->hasOne('App\Estatdetallcomanda','id','idestat');
	}
	
	public function idcomanda()
	{
		return $this->hasMany('App\Comanda','id','idcomanda');
	}
	
	public function producte()
	{
		return $this->hasOne('App\Producte','id','idproducte');
	}
	

}
