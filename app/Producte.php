<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Producte extends Model {

	protected $table = 'producte';
	
	public function tipusproducte()
	{
		return $this->hasOne('App\Tipusproducte','id','idtipus');
	}
	
	public function producte()
	{
		return $this->hasMany('App\Valoracio','idvaloracio', 'id');
	
	}
	
	
	
		
}