<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estatdetallcomanda extends Model {

	protected $table = 'estatdetallcomanda';
	
	public function detallcomanda()
	{
		return $this->belongsTo('App\Detallcomanda');
	}

}
