<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Rangempleat extends Model {

	protected $table = 'rangempleat';
	
	public function rangempleat()
	{
		return $this->belongsTo('Empleat');
	}
}
