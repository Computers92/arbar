<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Valoracio extends Model {

	protected $table = 'valoracio';
	
	public function producte()
	{
		return $this->belongsTo('App\Producte', 'idproducte', 'id');
	}
	
	public function usuari()
	{
		return $this->belongsTo('App\User', 'idusuari', 'id');
	}

}
