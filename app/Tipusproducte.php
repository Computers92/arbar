<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipusproducte extends Model {

	protected $table = 'tipusproducte';
	
	public function tipusproducte()
	{
		return $this->belongsTo('Producte');
	}

}
