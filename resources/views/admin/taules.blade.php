@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Gestió de taules</h1>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-lg-3 col-md-6">
							@if($tauleslliures > 0)												   
							<div class="panel panel-green">
							@else
							<div class="panel panel-red">
							@endif
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-tasks fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$tauleslliures}}</div>
											<div>Taules lliures</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="table-responsive">
									<table class="table table-bordered table-hover table-striped text-center">
										<thead>
											<tr >
												<th class='text-center'>Taula</th>
												<th class='text-center' data-sortable="true">Capacitat</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($taules as $taula)
											
												@if ($taula->estat === 0)
													<tr class="success">
												@else
													<tr class="danger">
												@endif
													<td>{{$taula->id}}</td>	
													<td>{{$taula->capacitat}}</td>
													<td>
														<a href="{{URL::action('AdminController@updateTaula', $taula)}}">
															<button class='btn btn-primary'>UPDATE</button>
														</a>
													</td>
														
											@endforeach
												
											</tr>
											
												
										</tbody>
									</table>
								</div>
							</div>
						
						
					</div>
					<!-- /.row -->


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
