@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Crea producte</h1>
							
						</div>
					</div>
						
					{!! Form::open(array('url' => 'crearNouProducte', 'method' => 'POST', 'files' => true)) !!} 

					<ul class="errors">
						@foreach($errors->all('<li>:message</li>') as $message)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					
					
					<div class="form-group">
						
						{!! Form::label('Nom') !!}
						{!! Form::text('nom', null, 
									array('required', 
									'class'=>'form-control', 
									'placeholder'=>'Nom del producte')) !!}
						<br>
						
						
						
						{!! Form::label('Descripció') !!}
						{!! Form::text('descripcio', null, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Descripció del producte')) !!}
						
						<br>
						{!! Form::label('Tipus') !!}
						{!! Form::select('tipus', $tipusproductes) !!}
						
						<br>
						<br>
						
						{!! Form::label('Preu') !!}
						{!! Form::text('preu', null, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Preu del producte')) !!}
						
						<br>
						
						{!! Form::label('Stock') !!}
						{!! Form::text('stock', null, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Stock inicial')) !!}

						<br>
						
						{!! Form::file('image') !!}
						
						
						
						<br>
						<div class="form-group">
							{!! Form::submit('Guardar', 
							array('class'=>'btn btn-primary')) !!}
						</div>
						{!! Form::close() !!}
						
					</div>



				</div>
				
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
