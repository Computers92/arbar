@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Comandes</h1>
						</div>
					</div>
					
					<div class="row">
						
						@foreach ($comandes as $comanda)
						
						<div class="col-lg-4 col-md-6">				   
							<div class="panel panel-warning">
							
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-list-alt fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge"></div>
											
											<div class='text-left'><b>Taula {{$comanda->idtaula}}</b> | Cambrer<b> {{$comanda->empleat->name}}</b></div>
											<div><b>Detalls</b></div>	
											<div>
												@foreach($detallscomandes as $detall)
													@if($detall->idcomanda == $comanda->id)
													{{$detall->producte->nom}}
												
												
													@if($detall->idestat == 2)
															
												<span class="fa fa-clock-o" style="color:orange"></span>
												<a href="{{URL::action('AdminController@updateEstatDetallComanda', $detall->id)}}">
													<span class="fa fa-arrow-circle-right" aria-hidden="true" style="color:orange"></span></a>
														@elseif($detall->idestat == 3)
															
												<span class="fa fa-check" aria-hidden="true" style="color:green"></span>
													
														@endif
														
														<br>
													@endif
												@endforeach
											
												<div class='text-left'>
													<br><a href="{{URL::action('AdminController@finalitzaComanda', $comanda->id)}}">
													<button class='btn btn-warning'>Finalitza Comanda</button>
													</a>
												</div>
												
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						
						
					</div>
						
					<!-- /.row -->


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
