@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')

<script src="{{URL::asset('js/jquery.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('js/plugins/morris/raphael.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/morris/morris.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/morris/morris-data.js')}}"></script>

<!-- Flot Charts JavaScript -->
<!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
<script src="{{URL::asset('js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/flot-data.js')}}"></script>

			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Estadistiques </h1>
						</div>
					</div>
					
					<div>
						<h2>Productes per categoria</h2>
						
						<div id="myfirtschart" style='height: 250px;'> </div>

						<script>
							
							new Morris.Line ({
								
							element: 'myfirtschart',
								data: [
									{ year: '2008', value: 20 },
									{ year: '2009', value: 10 },
									{ year: '2010', value: 40 },
									{ year: '2011', value: 60 },
									{ year: '2012', value: 20 }
									
								],
									
									xkey: 'year',
									ykeys: ['value'],
									labels: ['Value']
							});
							
														
							
						</script>
						
					
					</div>
					
				</div>
			</div>
			<!-- /#page-wrapper -->


@stop
