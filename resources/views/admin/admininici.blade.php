@extends('admin.master')

@section('title', 'ArBar - Admin')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Panell Principal</h1>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-3 col-md-6">
							@if($missatgespendents > 0)
							<div class="panel panel-info">
								@else
							<div class="panel panel-primary">
									@endif
							<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-comments fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$missatgespendents }}</div>
											<div>Noves Valoracions</div>
										</div>
									</div>
								</div>
								<a href="valoracionsadmin">
									<div class="panel-footer">
										<span class="pull-left">Consultar</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							@if($tauleslliures > 0)
								<div class="panel panel-green">
							@else
								<div class="panel panel-red">
							@endif
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-tasks fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$tauleslliures}}</div>
											<div>Taules lliures</div>
										</div>
									</div>
								</div>
								<a href="taulesadmin">
									<div class="panel-footer">
										<span class="pull-left">Consultar</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="panel panel-yellow">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-shopping-cart fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$comandespendents}}</div>
											<div>Comandes actives</div>
										</div>
									</div>
								</div>
								<a href="comandesadmin">
									<div class="panel-footer">
										<span class="pull-left">Veure</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							@if($productessensestock > 0)
							<div class="panel panel-red">
							@else
								<div class="panel panel-green">
							@endif
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-forumbee"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$productessensestock}}</div>
											<div>Productes sense stock</div>
										</div>
									</div>
								</div>
									<a href="{{URL::asset('productesadmin')}}">
									<div class="panel-footer">
										<span class="pull-left">Veure</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<!-- /.row -->

				
					<!-- /.row -->

					<!-- /.row -->

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
