@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Finalització de comanda</h1>
						</div>
					</div>
					
					<div class="row">
						
						
						
						<div class="col-lg-4 col-md-6">				   
							<div class="panel panel-warning ">
								<div class='text-center'>
									
								<h4>
								{{$empresa[0]->nom}}<br>
								{{$empresa[0]->cif}}</h4>
								Comanda {{$comanda->id}} - 
								{{ date('F d, Y', strtotime($comanda->created_at)) }} <br/>
								</div><br>
								<table class='table'>
									<thead>
										<tr>
											<th>Concepte</th>
											<th>Preu</th>
										</tr>
									</thead>
									<tbody>
										<?php $sum=0; ?>
										@foreach($detallcomandes as $detall)
										<tr id="tr_id_1" class="tr-class-1">
											<td id="td_id_1" class="td-class-1">{{$detall->producte->nom}}</td>
											<td>{{$detall->producte->preu}} €</td>
										</tr>
										
										@endforeach
									<tfoot>
										<tr>
											<td class='text-right'><b>Total</b></td>
											<td><b>{{$preufinal}} €</b><br></td>
											<td><span class='fa fa-print'></span></td>
										</tr>
									</tfoot>
								</table>
								
								
							</div>
						
						</div>
					
						
						
					</div>
						
					<!-- /.row -->


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
