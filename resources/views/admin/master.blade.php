
<html>
	<head>
		<title>@yield('title')</title>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Bootstrap Core CSS -->
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- Custom CSS -->		
		<link href="{{ URL::asset('css/sb-admin.css') }}" rel="stylesheet">

		<!-- Morris Charts CSS -->
		<link href="{{ URL::asset('css/plugins/morris.css') }}" rel="stylesheet">

		<!-- Custom Fonts -->
		<link href="{{ URL::asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
		
	</head>
	<body>
<div id="wrapper">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{URL::asset('admin')}}">{{$empresa[0]->nom}} / {{Auth::user()->name}}  </a>
				
			</div>
							
			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li class="active">
						<a href="admin"><i class="fa fa-fw fa-dashboard"></i>Panell</a>
						
					</li>
					<li>
						<a href="historicadmin"><i class="fa fa-fw fa-bar-chart-o"></i> Històric</a>
					</li>
					<li>
						<a href="usuarisadmin"><i class="fa fa-fw fa-table"></i> Usuaris</a>
					</li>
					<li>
						<a href="{{URL::asset('productesadmin')}}"><i class="fa fa-fw fa-edit"></i> Productes</a>
					</li>
					
					<li>
						<a href="{{URL::asset('estadistiques')}}"><i class="fa fa-fw fa-edit"></i> Estadistiques</a>
					</li>
					
					<li>
						<a href="{{URL::asset('auth/logout')}}"><i class="fa fa-fw fa-sign-out"></i> Sortir</a>
					</li>
					
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</nav>

			<div class="container">
				@yield('content')
			</div>
		</div>
	</body>
</html>
