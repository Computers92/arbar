@extends('admin.master')

@section('title', 'ArBar - Historic')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Històric de Comandes</h1>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-lg-3 col-md-6">			   
							<div class="panel panel-info">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-tasks fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$totalcomandes}}</div>
											<div>Comandes</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<table class="table text-left">
						<thead>
							<tr>
								<th>Num Comanda</th>
								<th>Usuari</th>
								<th>Preu</th>
								<th>Data</th>
								<th></th>
							</tr>
						</thead>
						@foreach ($comandes as $comanda)
						<tbody>
							<tr>
								<td>{{$comanda->id}}</td>
								<td>{{$comanda->usuari->name}}</td>
								<td>{{$comanda->preufinal->preu}} €</td>
								<td>{{$comanda->created_at}}</td>
							</tr>
						</tbody>
						@endforeach
					</table>
						


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
