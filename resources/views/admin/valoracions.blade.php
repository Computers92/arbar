@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Valoracions</h1>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-lg-3 col-md-6">
							@if($valoracionsnollegides > 0)		
							<div class="panel panel-primary">
							@else
							<div class="panel panel-green">
							@endif
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-comments fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$valoracionsnollegides}}</div>
											<div>Valoracions no llegides</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
						<div class="row">
							<div class="col-lg-6">
								<div class="table-responsive">
									<table class="table table-bordered table-hover table-striped text-center">
										<thead>
											<tr >
												<th class='text-center'>Estat</th>
												
												<th class='text-center'>Producte</th>
												<th class='text-center'>Qualificació</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@foreach ($valoracions as $valoracio)

												@if ($valoracio->llegida === 0)
												<tr class="info">
													<td><span class="fa fa-envelope"></span></td>
												@else
												<tr class="normal">
													<td class='fa fa-envelope-o'></td>
												@endif
													
													<td>{{$valoracio->producte->nom}}</td>
													<td>
														@for ($i = 0; $i < 5; $i++)
															@if ($valoracio->qualificacio > $i)
															<font class='fa fa-star'/>
															@else
															<font class='fa fa-star-o'/>
															@endif
														@endfor
													</td>
													<td>
														<a href="{{URL::action('AdminController@updateValoracio', $valoracio->id)}}"><button class='btn btn-primary'>Llegir	</button></a>
													</td>
												</tr>
											@endforeach

										</tbody>
									</table>
								</div>
							</div>
						
							
					</div>
					<!-- /.row -->


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
