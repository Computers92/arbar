@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Usuaris</h1>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-lg-3 col-md-6">
																		   
							<div class="panel panel-green">
							
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-user fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">{{$numusuaris}}</div>
											<div>Usuaris</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nom</th>
								<th>Mail</th>
								<th>Data d'unio</th>
							</tr>
						</thead>
						@foreach ( $usuaris as $usuari)
						<tbody>
							<tr>
								<td>{{$usuari->id}}</td>
								<td>{{$usuari->name}}</td>
								<td>{{$usuari->email}}</td>
								<td>{{$usuari->created_at}}</td>
								
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
