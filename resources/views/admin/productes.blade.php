@extends('admin.master')

@section('title', 'ArBar - Productes')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Productes</h1>
							<a href="{{ URL::to('nouproducte')}}"><button class='btn btn-primary'>Nou producte</button></a>
						</div>
					</div>
					
					<table class="table">
						<thead>
							<tr >
								<th></th>
								
								<th>Nom</th>
								<th>Tipus</th>
								<th>Descripció</th>
								<th>Stock</th>
							</tr>
						</thead>
						@foreach ( $productes as $prod)
						<tbody>
							@if(($prod->stock <= 5)&&($prod->stock > 1))
								<tr class="alert alert-warning">				
							@elseif($prod->stock == 0)
								<tr class="alert alert-danger">
							@elseif($prod->stock > 5)
								<tr class="alert alert-success">
							@endif
							
								<td class='text-center'><img src="{{ URL::asset($prod->img) }}"" " width='50'/></td>
								
								<td>{{$prod->nom}}</td>
								<td>{{$prod->tipusproducte->nom}}</td>
								<td>{{$prod->descripcio}}</td>
								<td>{{$prod->stock}}</td>
									
									<td><a href="{{ URL::action('AdminController@editaProducte', $prod->id)}}"><button class='btn btn-primary'>Modifica</button></a></td>
								
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
