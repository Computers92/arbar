@extends('admin.master')

@section('title', 'ArBar - Modifica producte')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Modifica producte</h1>
							
						</div>
					</div>
						
					{!! Form::open(array('url' => 'modificaProducteExistent', 'method' => 'POST', 'files' => true)) !!} 

					<ul class="errors">
						@foreach($errors->all('<li>:message</li>') as $message)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					
					
					<div class="form-group">
						
						{!! Form::hidden('idproducte', $producte->id) !!}
						
						{!! Form::label('Nom') !!}
						{!! Form::text('nom', $producte->nom, 
									array('required', 
									'class'=>'form-control', 
									'placeholder'=>'Nom del producte')) !!}
						<br>
						
						
						
						{!! Form::label('Descripció') !!}
						{!! Form::text('descripcio', $producte->descripcio, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Descripció del producte')) !!}
						
						<br>
						{!! Form::label('Tipus') !!}
						{!! Form::select('tipus', $tipusproductes, $producte->idtipus) !!}
						
						<br>
						<br>
						
						{!! Form::label('Preu') !!}
						{!! Form::text('preu', $producte->preu, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Preu del producte')) !!}
						
						<br>
						
						{!! Form::label('Stock') !!}
						{!! Form::text('stock', $producte->stock, 
						array('required', 
						'class'=>'form-control', 
						'placeholder'=>'Stock inicial')) !!}

						<br>
						<img class="img-responsive" src='{{URL::asset($producte->img)}}') width='10%'>
						{!! Form::file('image') !!}
						
						
						
						<br>
						<div class="form-group">
							{!! Form::submit('Guardar', 
							array('class'=>'btn btn-primary')) !!}
						</div>
						{!! Form::close() !!}
						
					</div>



				</div>
				
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->
@stop
