@extends('admin.master')

@section('title', 'ArBar - Taules')

@section('content')
			<div id="page-wrapper">

				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-9">
							<h1 class="page-header">Valoracio {{$valoracio[0]->id}}</h1>
						</div>
					</div>
					<dl>
						<dt>ID</dt>
						<dd>{{$valoracio[0]->id}}</dd>
						<br />
						<dt>Producte</dt>
						<dd>{{$valoracio[0]->producte->nom}}</dd>
						<br />
						<dt>Usuari</dt>
						<dd>{{$valoracio[0]->usuari->name}}</dd>
						<br />
						<dt>Qualificació</dt>
						<dd>
							@for ($i = 0; $i < 5; $i++)
							 @if($valoracio[0]->qualificacio > $i)
							<span class="fa fa-star" aria-hidden="true"></span>
							@else
							<span class="fa fa-star-o" aria-hidden="true"></span>
							@endif
							
							@endfor
						</dd>
						<br />
						<dt>Comentari</dt>
						<dd>{{$valoracio[0]->comentari}}</dd>
							
						
					</dl>
				</div>
			</div>
			<!-- /#page-wrapper -->
@stop
