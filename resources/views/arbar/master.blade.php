<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>@yield('title')</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link href="{{ URL::asset('css/settings.css') }}" rel="stylesheet">		
		<!-- FlexSlider Css -->
		<link rel="stylesheet" href="{{ URL::asset('css/flexslider.css') }}" media="screen" />
		<!-- Portfolio CSS -->
		<link href="{{ URL::asset('css/prettyPhoto.css') }}" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">	
		<!-- Custom Less -->
		<link href="{{ URL::asset('css/less-style.css') }}" rel="stylesheet">	
		<!-- Custom CSS -->
		<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
		<!--[if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif]-->

		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>

	<body>

		<!-- Shopping cart Modal -->
		<div class="modal fade" id="shoppingcart1" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cistella | Comanda {{$comanda[0]->id}} </h4>
					</div>
					<div class="modal-body">
						<!-- Items table -->
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Nom</th>
									<th>Preu</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($detallcomanda as $detall)

								<tr>
									<td><a href="#">{{$detall->producte->nom}}</a></td>

									<td>{{$detall->producte->preu}} €</td>
								</tr>

								@endforeach
								<tr>

									<th>Total</th>
									<th>{{$totalcomanda}} €</th>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Continuar comprant</button>
						<a href="{{URL::action('ArBarController@confirmacomanda')}}"><button type="button" class="btn btn-info">Demana!</button></a>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- Model End -->

		<!-- Page Wrapper -->
		<div class="wrapper">

			<!-- Header Start -->

			<div class="header">
				<div class="container">
					<!-- Header top area content -->
					<div class="header-top">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- Header top left content contact -->
								<div class="header-contact">
									<!-- Contact number -->
									<span><i class="fa fa-phone red"></i>{{Auth::user()->name}}</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<!-- Header top right content search box -->
							
							</div>
							<div class="col-md-4 col-sm-4">
								<!-- Button Kart -->
								<div class="btn-cart-md">
									<a class="cart-link" href="#">
										<!-- Image -->
										<img class="img-responsive" src="{{ URL::asset('img/cart.png') }}" alt="" />
										<!-- Heading -->
										<h4>Cistella</h4>
										<span>{{$totalcomanda}} €</span>
										<div class="clearfix"></div>
									</a>
									<ul class="cart-dropdown" role="menu">
										@foreach ($detallcomanda as $detall)
										<li>
											<!-- Cart items for shopping list -->
											<div class="cart-item">
												<!-- Item remove icon -->
												<a href="{{URL::action('ArBarController@borraElementComanda', $detall->id)}}"><i class="fa fa-times"></i></a>
												<!-- Image -->
												<img class="img-responsive img-rounded" src="{{ URL::asset($detall->producte->img) }}" alt="" />
												<!-- Title for purchase item -->
												<span class="cart-title"><a href="#">{{$detall->producte->nom}}</a></span>
												<!-- Cart item price -->
												<span class="cart-price pull-right red">{{$detall->producte->preu}} €</span>
												<div class="clearfix"></div>
											</div>
										</li>
										@endforeach

										<li>
											<!-- Cart items for shopping list -->
											<div class="cart-item">
												<a class="btn btn-danger" data-toggle="modal" href="#shoppingcart1">Demana!</a>
											</div>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-5">
							<!-- Link -->
							<a href="{{ URL::to('/')}}">
								<!-- Logo area -->
								<div class="logo">
									<img class="img-responsive" src="{{ URL::asset('img/logo.png')}}" alt="" />
									<!-- Heading -->
									<h1>ArBar</h1>
									<!-- Paragraph -->
									<p>Fàcil, ràpid</p>
								</div>
							</a>
						</div>
						<div class="col-md-8 col-sm-7">
							<!-- Navigation -->
							<nav class="navbar navbar-default navbar-right" role="navigation">
								<div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>
									</div>

									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<ul class="nav navbar-nav">
											<li><a href="{{ URL::to('/')}}"><img src="{{ URL::asset('img/nav-menu/nav1.jpg')}}" class="img-responsive" alt="" /> Inici</a></li>
											
											
										
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ URL::asset('img/nav-menu/nav4.jpg')}}" class="img-responsive" alt="" /> Compra <b class="caret"></b></a>
												<ul class="dropdown-menu">
													@foreach($tipusproductes as $tipus)
													<li><a href="{{URL::to('demana/'.$tipus->id.'')}}">{{$tipus->nom}}</a></li>
													@endforeach
													<li><a href="{{URL::to('demana/0')}}">Tots</a></li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ URL::asset('img/nav-menu/nav5.jpg')}}" class="img-responsive" alt="" /> Perfil <b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a href="{{URL::to('historiccomandes')}}">Històric comandes</a></li>
													<li><a href="{{URL::to('valoracionsfetes')}}">Valoracions</a></li>
													<li><a href="{{URL::to('perfilusuari')}}">Perfil</a></li>
													<li><a href="{{URL::to('auth/logout')}}">Tancar sessió</a></li>
													
												</ul>
											</li>
											
										</ul>
									</div><!-- /.navbar-collapse -->
								</div><!-- /.container-fluid -->
							</nav>
						</div>
					</div>
					
				</div> <!-- / .container -->
			</div>

			<!-- Header End -->
			
		@yield('content')
			<!-- Footer Start -->

			<div class="footer padd">
				<div class="container">
					<div class="row">
						
						
						<div class="clearfix visible-sm"></div>
						
						<div class="col-md-12 col-sm-6">
							<!-- Footer widget -->
							<div class="footer-widget">
								<!-- Heading -->
								<h4>Informació</h4>
								<div class="contact-details">
									<!-- Address / Icon -->
									<i class="fa fa-map-marker br-red"></i> <span>Enric prat de les moreres 77<br />08401 - Granollers</span>
									<div class="clearfix"></div>
									<!-- Contact Number / Icon -->
									<i class="fa fa-phone br-green"></i> <span>+91 88-88-888888</span>
									<div class="clearfix"></div>
									<!-- Email / Icon -->
									<i class="fa fa-envelope-o br-lblue"></i> <span><a href="#">info@arbar.com</a></span>
									<div class="clearfix"></div>
								</div>
								<!-- Social media icon -->
								
							</div> <!--/ Footer widget end -->
						</div>
					</div>
					<!-- Copyright -->
					<div class="footer-copyright">
						<!-- Paragraph -->
						<p>&copy; Copyright 2014 <a href="#">ArBar</a></p>
					</div>
				</div>
			</div>

			<!-- Footer End -->

			<!-- Scroll to top -->
			<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 

		
		</div>
		<!-- Javascript files -->
		<!-- jQuery -->
		<script src="{{ URL::asset('js/jquery.js')}}"></script>
		<!-- Bootstrap JS -->
		<script src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
		<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
		<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.plugins.min.js')}}"></script>
		<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.revolution.min.js')}}"></script>
		<!-- FLEX SLIDER SCRIPTS  -->
		<script defer src="{{ URL::asset('js/jquery.flexslider-min.js')}}"></script>
		<!-- Pretty Photo JS -->
		<script src="{{ URL::asset('js/jquery.prettyPhoto.js')}}"></script>
		<!-- Respond JS for IE8 -->
		<script src="{{ URL::asset('js/respond.min.js')}}"></script>
		<!-- HTML5 Support for IE -->
		<script src="{{ URL::asset('js/html5shiv.js')}}"></script>
		<!-- Custom JS -->
		<script src="{{ URL::asset('js/custom.js')}}"></script>
		<!-- JS code for this page -->
		<script>
			/* ******************************************** */
			/*  JS for SLIDER REVOLUTION  */
			/* ******************************************** */
			jQuery(document).ready(function() {
				jQuery('.tp-banner').revolution(
					{
						delay:9000,
						startheight:500,

						hideThumbs:10,

						navigationType:"bullet",	

						hideArrowsOnMobile:"on",

						touchenabled:"on",
						onHoverStop:"on",

						navOffsetHorizontal:0,
						navOffsetVertical:20,

						stopAtSlide:-1,
						stopAfterLoops:-1,

						shadow:0,

						fullWidth:"on",
						fullScreen:"off"
					});
			});
			/* ******************************************** */
			/*  JS for FlexSlider  */
			/* ******************************************** */

			$(window).load(function(){
				$('.flexslider-recent').flexslider({
					animation:		"fade",
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
				$('.flexslider-testimonial').flexslider({
					animation: 		"fade",
					slideshowSpeed:	5000,
					animationSpeed:	1000,
					controlNav:		true,
					directionNav:	false
				});
			});

			/* Gallery */

			jQuery(".gallery-img-link").prettyPhoto({
				overlay_gallery: false, social_tools: false
			});

		</script>
	</body>	
	
</html>