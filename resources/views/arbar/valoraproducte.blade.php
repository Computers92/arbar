@extends('arbar.master')

@section('title', 'ArBar - Valora producte')

@section('content')

			<!-- Inner Content -->
			<div class="inner-page padd">
			
				<!-- Single Item Start -->
				
				<div class="single-item">
					<div class="container">
						<!-- Shopping single item contents -->
						<div class="single-item-content">
							<div class="row">
								<div class="col-md-4 col-sm-5">
									<!-- Product image -->
									<img class="img-responsive img-thumbnail" src="{{ URL::asset($producte->img)}}	" alt="" />
								</div>
								<div class="col-md-8 col-sm-7">
									<!-- Heading -->
									<h3>{{$producte->nom}}</h3>
									<div class="row">
										<div class="col-md-7 col-sm-12">
											<!-- Single item details -->
											<div class="item-details">
												<!-- Paragraph -->
												<p class="text-justify">
													{{$producte->descripcio}}
												</p>
												<!-- Heading -->
											</div>
										</div>
										<div class="col-md-5 col-sm-12"> 
											<!-- Form inside table wrapper -->
											<div class="table-responsive">
												<!-- Ordering form -->
												{!! Form::open(array('url' => 'ferValoracioProducte', 'method' => 'POST'))!!} 
													<!-- Table -->
													<table class="table table-bordered">
														<tr>
															<td>Preu</td>
															<td>{{$producte->preu}} €</td>
														</tr>
														<tr>
															<td>Tipus</td>
															<td>{{$producte->tipusproducte->nom}}</td>
														</tr>
														
														<tr>
															<td>Valoracio</td>
															<td><div class="form-group">
																
																{!! Form::select('estrelles', [1, 2, 3, 4, 5]) !!} 
															
															</div></td>
														</tr>
														<tr>
															<td>Comentari</td>
															<td>
																{!! Form::text('comentari', null, 
																array('required', 
																'class'=>'form-control', 
																'placeholder'=>'Comentari')) !!}
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td><div class="form-group">
																<button type="submit" class="btn btn-primary ">Valora</button>
															</div></td>
														</tr>
													</table>
												{!! Form::hidden('idproducte', $producte->id) !!}
												{!! Form::close() !!}
											</div><!--/ Table responsive class end -->
										</div>
									</div><!--/ Inner row end  -->
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Single Item End -->
			
					
			
			@stop