@extends('arbar.master')

@section('title', 'ArBar - Productes')

@section('content')

			
	<!-- Banner Start -->
	<div class="banner padd">
		<div class="container">
			<!-- Image -->
			<img class="img-responsive" src="{{ URL::asset('img/crown-white.png')}}" alt="" />
			<!-- Heading -->
			<h2 class="white">Les teves valoracions</h2>
			
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- Inner Content -->
	<div class="inner-page padd">

		<!-- Shopping Start -->

		<div class="shopping">
			<div class="container">
				<!-- Shopping items content -->
				<div class="shopping-content">
					<div class="row">
						
						
						
						@foreach($valoracions as $val)
						<div class="col-md-3 col-sm-6">
							<!-- Shopping items -->
							<div class="shopping-item">
								<!-- Image -->
								<img class="img-responsive" src="{{ URL::asset($val->producte->img) }}" width="355" />
								<!-- Shopping item name / Heading -->
								<h4 class="pull-left">{{$val->producte->nom}}</h4>
								
								<div class="clearfix"></div>
								<!-- Paragraph -->
								<p>{{$val->qualificacio}}/5 | {{$val->comentari}}</p>
								
								<!-- Buy now button -->
								<div class="visible-xs">
									<a class="btn btn-danger btn-sm" href="#">Buy Now</a>
								</div>
								<!-- Shopping item hover block & link -->
								<div class="item-hover hidden-xs"></div>
								
							</div>
						</div>
						@endforeach
						
						@if($valoracions=='[]')
						<div class="alert alert-warning" role="alert"><b>Ops!</b> No has fet cap valoració!</div>

						@endif
						
				</div>
			</div>
		</div>

		
	</div><!-- / Inner Page Content End -->	

	
		
		
		@stop
		