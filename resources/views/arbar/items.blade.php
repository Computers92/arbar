@extends('arbar.master')

@section('title', 'ArBar - Productes')

@section('content')

			
	<!-- Banner Start -->
	<div class="banner padd">
		<div class="container">
			<!-- Image -->
			<img class="img-responsive" src="{{ URL::asset('img/crown-white.png')}}" alt="" />
			<!-- Heading -->
			<h2 class="white">Compra</h2>
			<ol class="breadcrumb"> 
				<li class="active">{{$nomtipus}}</li>
			</ol>
				
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- Inner Content -->
	<div class="inner-page padd">

		<!-- Shopping Start -->

		<div class="shopping">
			<div class="container">
				<!-- Shopping items content -->
				<div class="shopping-content">
					<div class="row">
						
						
						
						@foreach($productes as $producte)
						<div class="col-md-3 col-sm-6">
							<!-- Shopping items -->
							<div class="shopping-item">
								<!-- Image -->
								<a href="item-single.html"><img class="img-responsive" src="{{ URL::asset($producte->img) }}" width="355" /></a>
								<!-- Shopping item name / Heading -->
								<h4 class="pull-left"><a href="item-single.html">{{$producte->nom}}</a></h4>
								<span class="item-price pull-right">{{$producte->preu}} €</span>
								<div class="clearfix"></div>
								<!-- Paragraph -->
								<p>{{$producte->descripcio}}</p>
								<!-- Buy now button -->
								<div class="visible-xs">
									<a class="btn btn-danger btn-sm" href="#">Buy Now</a>
								</div>
								<!-- Shopping item hover block & link -->
								<div class="item-hover br-red hidden-xs"></div>
								<a class="link hidden-xs" href="{{URL::action('ArBarController@afegeixElementComanda', $producte->id)}}">Afegir</a>
							</div>
						</div>
						@endforeach
						
						@if($productes=='[]')
						<div class="alert alert-warning" role="alert"><b>Ops!</b> No hi ha productes disponibles!</div>

						@endif
						
						
					<!-- Pagination
					<div class="shopping-pagination">
						<ul class="pagination">
							<li class="disabled"><a href="#">&laquo;</a></li>
							<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>
					<!-- Pagination end-->
				</div>
			</div>
		</div>

		
	</div><!-- / Inner Page Content End -->	

	
		
		
		@stop
		