@extends('arbar.master')

@section('title', 'ArBar - Històric Comandes')

@section('content')
	
	<body>
			
			<!-- Header End -->
			
			<!-- Banner Start -->
			
			<div class="banner padd">
				<div class="container">
					<!-- Image -->
					<img class="img-responsive" src="img/crown-white.png" alt="" />
					<!-- Heading -->
					<h2 class="white">Comandes</h2>
					<ol class="breadcrumb">
						<li>Actives / històric</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Banner End -->
			
			<!-- Inner Content -->
			
				<!-- Inner page menu start -->
				<div class="inner-menu">
					<div class="container">
						<div class="row">
							@foreach($comandes as $comand)
							
							
							<div class="col-md-4 col-sm-6">
								<!-- Inner page menu list -->
								
								<div class="menu-list">
									<!-- Menu item heading -->
									<h3>Comanda {{$comand->id}} |<small> {{$comand->created_at->format('d/m/y')}} </small> </h3>
									
									<!-- Menu list items -->
									@foreach($detallscomandes as $detail)
									@if($detail->idcomanda == $comand->id)
									<div class="menu-list-item">
										<!-- Heading / Dish name -->
										<h4 class="pull-left">{{$detail->producte->nom}}</h4>
										<!-- Dish price -->
										<a href="{{URL::action('ArBarController@valoraProducte', $detail->producte->id)}}">
										<button class="btn btn-primary pull-right">valora</button>
										</a>
										<div class="clearfix"></div>
									</div>
									@endif
									@endforeach
								</div>
							</div>
							
							@endforeach
						</div>
					</div>
				
				
				
			
			@stop