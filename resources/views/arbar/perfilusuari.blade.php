@extends('arbar.master')

@section('title', 'ArBar - Valora producte')

@section('content')

			<!-- Inner Content -->
			<div class="inner-page padd">
			
				<!-- Single Item Start -->
				
				<div class="single-item">
					<div class="container">
						<!-- Shopping single item contents -->
						<div class="single-item-content">
							<div class="row">
								<div class="col-md-4 col-sm-5">
									<!-- Product image -->
									<img class="img-responsive img-thumbnail" src="{{URL::asset('img/user.jpg')}}" alt="" />
								</div>
								<div class="col-md-8 col-sm-7">
									<!-- Heading -->
									
									<div class="row">
										
										<div class="col-md-12 col-sm-12"> 
											<!-- Form inside table wrapper -->
											<div class="table-responsive">
												<!-- Ordering form -->
												
													<!-- Table -->
													<table class="table table-bordered">
														<tr>
															<td>Nom usuari</td>
															<td>{{$usuari->name}}</td>
														</tr>
														<tr>
															<td>Mail</td>
															<td>{{$usuari->email}}</td>
														</tr>
														
														<tr>
															<td>Valoracions fetes</td>
															<td>{{$valoracionsfetes}}</td>
														</tr>
														<tr>
															<td>Comandes fetes</td>
															<td>{{$comandesfetes}}</td>
														</tr>
													
													</table>
											
											</div><!--/ Table responsive class end -->
										</div>
									</div><!--/ Inner row end  -->
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Single Item End -->
			
					
			
			@stop