<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
		
		<!-- Title here -->
		
		<!-- Description, Keywords and Author -->
		
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link href="{{ URL::asset('css/settings.css') }}" rel="stylesheet">		
		<!-- FlexSlider Css -->
		<link rel="stylesheet" href="{{ URL::asset('css/flexslider.css') }}" media="screen" />
		<!-- Portfolio CSS -->
		<link href="{{ URL::asset('css/prettyPhoto.css') }}" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">	
		<!-- Custom Less -->
		<link href="{{ URL::asset('css/less-style.css') }}" rel="stylesheet">	
		<!-- Custom CSS -->
		<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
		<!--[if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif]-->

		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	
</head>
<body>
	<div class="header">
		<div class="container">
			<!-- Header top area content -->
			
			<div class="row">
				<div class="col-md-4 col-sm-5">
					<!-- Link -->
					<a href="index.html">
						<!-- Logo area -->
						<div class="logo">
							<img class="img-responsive" src="{{ URL::asset('img/logo.png')}}" alt="" />
							<!-- Heading -->
							<h1>ArBar</h1>
							<!-- Paragraph -->
							<p>Fàcil, ràpid</p>
						</div>
					</a>
				</div>
				
			</div>
		</div> <!-- / .container -->
	</div>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
