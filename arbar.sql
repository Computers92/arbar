-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12-06-2015 a les 16:39:13
-- Versió del servidor: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arbar`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `comanda`
--

CREATE TABLE IF NOT EXISTS `comanda` (
`id` int(10) unsigned NOT NULL,
  `idusuari` int(10) unsigned NOT NULL,
  `idtaula` int(10) unsigned NOT NULL,
  `idempleat` int(10) unsigned NOT NULL,
  `estat` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Bolcant dades de la taula `comanda`
--

INSERT INTO `comanda` (`id`, `idusuari`, `idtaula`, `idempleat`, `estat`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 1, 1, '2015-05-14 13:15:06', '2015-05-22 20:22:02'),
(4, 2, 2, 1, 1, '2015-05-13 10:23:29', '2015-05-18 14:50:05'),
(5, 1, 2, 2, 1, '2015-05-23 08:02:02', '2015-05-26 19:11:01'),
(7, 3, 3, 1, 1, '2015-05-27 16:28:56', '2015-05-27 16:28:56'),
(9, 6, 3, 1, 1, '2015-05-27 16:41:40', '2015-05-27 16:43:28'),
(10, 6, 3, 1, 1, '2015-05-27 16:44:42', '2015-05-27 16:44:42'),
(12, 1, 3, 1, 1, '2015-05-28 14:30:02', '2015-05-28 15:24:42'),
(14, 1, 3, 1, 1, '2015-05-28 14:31:50', '2015-05-28 14:31:50'),
(15, 1, 3, 1, 1, '2015-05-28 15:04:15', '2015-05-28 15:04:15'),
(16, 1, 3, 1, 1, '2015-05-28 15:18:07', '2015-05-28 15:18:07'),
(17, 1, 3, 1, 0, '2015-05-29 14:16:38', '2015-05-29 14:16:38'),
(18, 1, 3, 1, 0, '2015-05-31 14:59:27', '2015-05-31 14:59:27');

-- --------------------------------------------------------

--
-- Estructura de la taula `detallcomanda`
--

CREATE TABLE IF NOT EXISTS `detallcomanda` (
`id` int(10) unsigned NOT NULL,
  `idcomanda` int(10) unsigned NOT NULL,
  `idproducte` int(10) unsigned NOT NULL,
  `idestat` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Bolcant dades de la taula `detallcomanda`
--

INSERT INTO `detallcomanda` (`id`, `idcomanda`, `idproducte`, `idestat`, `created_at`, `updated_at`) VALUES
(4, 4, 1, 3, '0000-00-00 00:00:00', '2015-05-18 13:21:08'),
(12, 3, 5, 3, '0000-00-00 00:00:00', '2015-05-22 20:42:00'),
(13, 3, 11, 3, '0000-00-00 00:00:00', '2015-05-22 20:42:00'),
(14, 3, 2, 3, '0000-00-00 00:00:00', '2015-05-22 20:57:19'),
(15, 5, 15, 3, '0000-00-00 00:00:00', '2015-05-26 19:09:04'),
(16, 5, 6, 3, '0000-00-00 00:00:00', '2015-05-26 19:10:55'),
(17, 5, 10, 3, '0000-00-00 00:00:00', '2015-05-26 19:10:56'),
(18, 5, 14, 3, '0000-00-00 00:00:00', '2015-05-27 14:35:20'),
(21, 5, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 9, 1, 3, '0000-00-00 00:00:00', '2015-05-27 16:43:17'),
(23, 9, 6, 3, '0000-00-00 00:00:00', '2015-05-27 16:43:19'),
(24, 9, 4, 3, '0000-00-00 00:00:00', '2015-05-27 16:43:20'),
(25, 12, 6, 3, '0000-00-00 00:00:00', '2015-05-28 15:23:51'),
(26, 12, 1, 3, '0000-00-00 00:00:00', '2015-05-28 15:23:49'),
(27, 12, 4, 3, '0000-00-00 00:00:00', '2015-05-28 15:24:42');

-- --------------------------------------------------------

--
-- Estructura de la taula `empleat`
--

CREATE TABLE IF NOT EXISTS `empleat` (
`id` int(10) unsigned NOT NULL,
  `idempresa` int(10) unsigned NOT NULL,
  `idrang` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Bolcant dades de la taula `empleat`
--

INSERT INTO `empleat` (`id`, `idempresa`, `idrang`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Kiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 1, 'Pep', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
`id` int(10) unsigned NOT NULL,
  `cif` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Bolcant dades de la taula `empresa`
--

INSERT INTO `empresa` (`id`, `cif`, `nom`, `created_at`, `updated_at`) VALUES
(1, '47813422X', 'ArBar', '2015-05-13 12:42:59', '2015-05-13 12:42:59');

-- --------------------------------------------------------

--
-- Estructura de la taula `estatdetallcomanda`
--

CREATE TABLE IF NOT EXISTS `estatdetallcomanda` (
`id` int(10) unsigned NOT NULL,
  `descripcio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Bolcant dades de la taula `estatdetallcomanda`
--

INSERT INTO `estatdetallcomanda` (`id`, `descripcio`, `created_at`, `updated_at`) VALUES
(1, 'Cistella', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Pendent', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Servit', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Bolcant dades de la taula `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_05_11_164122_create_empresa_table', 1),
('2015_05_11_171322_create_rangempleats_table', 1),
('2015_05_11_171323_create_empleats_table', 1),
('2015_05_13_072436_create_tipusproductes_table', 1),
('2015_05_13_081417_create_taulas_table', 1),
('2015_05_13_081713_create_comandas_table', 1),
('2015_05_13_082228_create_productes_table', 1),
('2015_05_13_082231_create_estatdetallcomandas_table', 1),
('2015_05_13_082232_create_detallcomandas_table', 1),
('2015_05_13_082709_create_valoracios_table', 1),
('2015_05_14_140917_create_usersrols_table', 2),
('2015_05_18_145916_create_preufinalcomandas_table', 3);

-- --------------------------------------------------------

--
-- Estructura de la taula `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de la taula `preufinalcomanda`
--

CREATE TABLE IF NOT EXISTS `preufinalcomanda` (
`id` int(10) unsigned NOT NULL,
  `idcomanda` int(10) unsigned NOT NULL,
  `preu` double(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Bolcant dades de la taula `preufinalcomanda`
--

INSERT INTO `preufinalcomanda` (`id`, `idcomanda`, `preu`, `created_at`, `updated_at`) VALUES
(2, 3, 3.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 4, 1.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, 13.90, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, 12.40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 5, 12.40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 5, 12.40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 5, 12.40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 5, 12.40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 9, 11.90, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 12, 11.90, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `preufinalcomandas`
--

CREATE TABLE IF NOT EXISTS `preufinalcomandas` (
  `idcomanda` int(10) unsigned NOT NULL,
  `preu` double(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de la taula `producte`
--

CREATE TABLE IF NOT EXISTS `producte` (
`id` int(10) unsigned NOT NULL,
  `idtipus` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `preu` double(8,2) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Bolcant dades de la taula `producte`
--

INSERT INTO `producte` (`id`, `idtipus`, `nom`, `descripcio`, `preu`, `img`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 'CocaCola', 'Beguda gasosa', 0.90, 'img/productes/coca-cola.png', 38, '2015-05-13 12:42:59', '2015-05-28 14:37:15'),
(2, 1, 'Fanta Naranja', 'Beguda gaseosa', 0.90, 'img/productes/fantanaranja.png', 40, '2015-05-23 02:16:17', '0000-00-00 00:00:00'),
(4, 5, 'Coulant Xocolata', 'Madalena calenta', 3.50, '/img/productes/coulan.png', 8, '2015-05-20 05:20:09', '2015-05-28 14:37:20'),
(5, 3, 'Spaguetti Bolonyesa', 'Spaguettis tradicionals italians', 7.20, 'img/productes/spaguettibolonesa.png', 16, '2015-05-15 07:23:23', '0000-00-00 00:00:00'),
(6, 3, 'Spaguetti Carbonara', 'Spaguetti amb crema de llet', 7.50, 'img/productes/spaguetticarbonara.png', 28, '2015-05-21 22:00:00', '2015-05-28 14:37:11'),
(10, 2, 'Patates Braves', 'Patates amb salsa brava', 4.00, '/img/productes/img_patatas_bravas_grande.png', 30, '2015-05-15 07:22:15', '0000-00-00 00:00:00'),
(11, 2, 'Croquetes carn', 'Croquetes de carn d''olla', 6.70, '/img/productes/croquetescarn.png', 200, '2015-05-22 07:24:03', '0000-00-00 00:00:00'),
(12, 4, 'Pollastre amb patates', 'Pollastre fregit amb patates', 6.60, '/img/productes/pollofrito.png', 12, '2015-05-22 07:07:06', '0000-00-00 00:00:00'),
(13, 4, 'Calamars romana', 'Calamars fregits a la romana', 6.00, '/img/productes/anillas_de_calamar.png', 10, '2015-05-22 00:25:28', '2015-05-27 16:51:23'),
(14, 3, 'Paella', 'Paella valenciana', 8.00, '/img/productes/fondo_paella.png', 15, '2015-05-21 02:14:14', '0000-00-00 00:00:00'),
(15, 1, 'Bitter Kas', 'Beguda refrescant', 0.90, '/img/productes/bitterkas.png', 0, '2015-05-20 15:16:24', '2015-05-27 15:20:52'),
(16, 3, 'Feideus', 'Fiedeus xinoesos', 8.90, '/img/productes/dish5.jpg', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 3, 'Pizza Margarita', 'Pizza típica', 7.50, '/img/productes/dish8.jpg', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `rangempleat`
--

CREATE TABLE IF NOT EXISTS `rangempleat` (
`id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Bolcant dades de la taula `rangempleat`
--

INSERT INTO `rangempleat` (`id`, `nom`, `descripcio`, `created_at`, `updated_at`) VALUES
(1, 'Cambrer', 'Servir taules', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `taula`
--

CREATE TABLE IF NOT EXISTS `taula` (
`id` int(10) unsigned NOT NULL,
  `capacitat` int(11) NOT NULL,
  `estat` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Bolcant dades de la taula `taula`
--

INSERT INTO `taula` (`id`, `capacitat`, `estat`, `created_at`, `updated_at`) VALUES
(1, 5, 1, '0000-00-00 00:00:00', '2015-05-21 16:00:40'),
(2, 2, 0, '0000-00-00 00:00:00', '2015-05-28 15:20:01'),
(3, 10, 0, '0000-00-00 00:00:00', '2015-05-28 15:20:04'),
(4, 2, 0, '0000-00-00 00:00:00', '2015-05-28 15:20:02');

-- --------------------------------------------------------

--
-- Estructura de la taula `tipusproducte`
--

CREATE TABLE IF NOT EXISTS `tipusproducte` (
`id` int(10) unsigned NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Bolcant dades de la taula `tipusproducte`
--

INSERT INTO `tipusproducte` (`id`, `nom`, `descripcio`, `created_at`, `updated_at`) VALUES
(1, 'Beguda', 'Beguda', '2015-05-13 12:42:59', '2015-05-13 12:42:59'),
(2, 'Aperitius', 'Per anar fent boca', '2015-05-13 12:42:59', '2015-05-13 12:42:59'),
(3, 'Primers plats', 'Primers plats', '2015-05-13 12:42:59', '2015-05-13 12:42:59'),
(4, 'Segons plats', 'Segons plats', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Postres', 'Postres', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Tapes', 'Tapes per picar', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de la taula `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Bolcant dades de la taula `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'atorrillas', 'atorrillas.92@gmail.com', '$2y$10$iPOu5bt6Vx4AFTAH92e0huIbJXFr6oWniM7LjW.B8yzWH41AKZIO2', 'WCYwGAGVf9srVYge2F4tAhGHG60u5Al2qHg6h4w6P2HxThCT0a5MyRLQY1Qp', '2015-05-13 12:44:00', '2015-05-28 15:04:13'),
(2, 'Fernando Alonso', 'fernando@alonso.com', '$2y$10$fM9ctbiUwtDu9uLYS/qgMOqT1E2pMQI3TSKDp9ZBDvvNA7PXswhIi', 'c9J62VXkR5EOf6JyskqXAuJ4Q7PAGtYsXo14ZSSqRZYy1A5tNrgpzn1ItZXg', '2015-05-14 10:14:33', '2015-05-14 12:00:57'),
(3, 'Kiko', 'kfernandez@gmail.com', '$2y$10$LV4nwrNjSqPHooCbUNnDDOoFifw3lYyB.h6HWNpzzsP1QwJB85Xfi', 'd9F97hFMdCT48bu8TGju9MdrbCePBzqykMSoKC2cTIclOwTpyDh2CexrpugL', '2015-05-27 16:05:21', '2015-05-27 16:38:07'),
(4, 'Bernat Huertas', 'bhuertas@gmail.com', '$2y$10$uTZR7TDzqQBZWlccpkRN8enIAOcsj2h7uF7cUW4trmg295cXmDd5G', 'WyDSb6qkgJO8fFHG2gdyA0CamyIDXSeXYCwoDeMGRuAS1xmEJhAh1M5kSP9L', '2015-05-27 16:06:39', '2015-05-27 16:07:58'),
(5, 'Francisco Wamba', 'fwamba@hotmail.com', '$2y$10$QhabILW6gbb4MK2YQ.mL/ePBFAb7yzn/sM1GEwwIvE3OcqT8V1vUi', 'qcL9GGwXyeSXTDsR2SLqZ8tWTYOVI8ZFu8O1ar0Ou870EPtTGlLycKueqgdg', '2015-05-27 16:08:22', '2015-05-27 16:09:52'),
(6, 'Bernat', 'bhuertas@yahoo.es', '$2y$10$T2/Drx8w39pKpeS6RZOjMOUzv8I/RTReCKJ2N40gd24mEcjh/obmy', '7X0cj5brqMA6xws2SH8b3vQrdTbNAXeDhXQuSu4UeQYHo4dxGle023uAseB6', '2015-05-27 16:40:52', '2015-05-27 16:44:30');

-- --------------------------------------------------------

--
-- Estructura de la taula `usersrols`
--

CREATE TABLE IF NOT EXISTS `usersrols` (
`id` int(10) unsigned NOT NULL,
  `iduser` int(10) unsigned NOT NULL,
  `idrol` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de la taula `valoracio`
--

CREATE TABLE IF NOT EXISTS `valoracio` (
`id` int(10) unsigned NOT NULL,
  `idproducte` int(10) unsigned NOT NULL,
  `idusuari` int(10) unsigned NOT NULL,
  `qualificacio` int(11) NOT NULL,
  `comentari` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `llegida` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Bolcant dades de la taula `valoracio`
--

INSERT INTO `valoracio` (`id`, `idproducte`, `idusuari`, `qualificacio`, `comentari`, `llegida`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 'Molt gustos', 1, '0000-00-00 00:00:00', '2015-05-14 08:16:45'),
(3, 1, 1, 5, 'Molt refrescant!', 1, '0000-00-00 00:00:00', '2015-05-14 08:09:12'),
(4, 2, 1, 1, 'No estava freda', 1, '0000-00-00 00:00:00', '2015-05-14 08:09:42'),
(5, 2, 2, 2, 'Una merda', 1, '0000-00-00 00:00:00', '2015-05-14 13:28:28'),
(6, 1, 2, 3, 'Una merda', 1, '0000-00-00 00:00:00', '2015-05-14 13:28:31'),
(7, 5, 1, 2, 'Poca quantitat', 1, '0000-00-00 00:00:00', '2015-05-26 19:07:12'),
(8, 2, 1, 4, 'Molt refrescant', 1, '0000-00-00 00:00:00', '2015-05-27 14:35:43'),
(9, 2, 1, 3, 'Freda', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 6, 6, 3, 'estaven una mica freds', 1, '0000-00-00 00:00:00', '2015-05-27 16:45:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comanda`
--
ALTER TABLE `comanda`
 ADD PRIMARY KEY (`id`), ADD KEY `comanda_idusuari_index` (`idusuari`), ADD KEY `comanda_idtaula_index` (`idtaula`), ADD KEY `comanda_idempleat_index` (`idempleat`);

--
-- Indexes for table `detallcomanda`
--
ALTER TABLE `detallcomanda`
 ADD PRIMARY KEY (`id`), ADD KEY `detallcomanda_idcomanda_index` (`idcomanda`), ADD KEY `detallcomanda_idproducte_index` (`idproducte`), ADD KEY `detallcomanda_idestat_index` (`idestat`);

--
-- Indexes for table `empleat`
--
ALTER TABLE `empleat`
 ADD PRIMARY KEY (`id`), ADD KEY `empleat_idempresa_index` (`idempresa`), ADD KEY `empleat_idrang_index` (`idrang`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estatdetallcomanda`
--
ALTER TABLE `estatdetallcomanda`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `preufinalcomanda`
--
ALTER TABLE `preufinalcomanda`
 ADD PRIMARY KEY (`id`), ADD KEY `preufinalcomanda_idcomanda_index` (`idcomanda`);

--
-- Indexes for table `producte`
--
ALTER TABLE `producte`
 ADD PRIMARY KEY (`id`), ADD KEY `producte_idtipus_index` (`idtipus`);

--
-- Indexes for table `rangempleat`
--
ALTER TABLE `rangempleat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taula`
--
ALTER TABLE `taula`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipusproducte`
--
ALTER TABLE `tipusproducte`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `usersrols`
--
ALTER TABLE `usersrols`
 ADD PRIMARY KEY (`id`), ADD KEY `usersrols_iduser_index` (`iduser`);

--
-- Indexes for table `valoracio`
--
ALTER TABLE `valoracio`
 ADD PRIMARY KEY (`id`), ADD KEY `valoracio_idproducte_index` (`idproducte`), ADD KEY `valoracio_idusuari_index` (`idusuari`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comanda`
--
ALTER TABLE `comanda`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `detallcomanda`
--
ALTER TABLE `detallcomanda`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `empleat`
--
ALTER TABLE `empleat`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `estatdetallcomanda`
--
ALTER TABLE `estatdetallcomanda`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `preufinalcomanda`
--
ALTER TABLE `preufinalcomanda`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `producte`
--
ALTER TABLE `producte`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `rangempleat`
--
ALTER TABLE `rangempleat`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taula`
--
ALTER TABLE `taula`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tipusproducte`
--
ALTER TABLE `tipusproducte`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usersrols`
--
ALTER TABLE `usersrols`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `valoracio`
--
ALTER TABLE `valoracio`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Restriccions per taules bolcades
--

--
-- Restriccions per la taula `comanda`
--
ALTER TABLE `comanda`
ADD CONSTRAINT `comanda_idempleat_foreign` FOREIGN KEY (`idempleat`) REFERENCES `empleat` (`id`),
ADD CONSTRAINT `comanda_idtaula_foreign` FOREIGN KEY (`idtaula`) REFERENCES `taula` (`id`),
ADD CONSTRAINT `comanda_idusuari_foreign` FOREIGN KEY (`idusuari`) REFERENCES `users` (`id`);

--
-- Restriccions per la taula `detallcomanda`
--
ALTER TABLE `detallcomanda`
ADD CONSTRAINT `detallcomanda_idcomanda_foreign` FOREIGN KEY (`idcomanda`) REFERENCES `comanda` (`id`),
ADD CONSTRAINT `detallcomanda_idestat_foreign` FOREIGN KEY (`idestat`) REFERENCES `estatdetallcomanda` (`id`),
ADD CONSTRAINT `detallcomanda_idproducte_foreign` FOREIGN KEY (`idproducte`) REFERENCES `producte` (`id`);

--
-- Restriccions per la taula `empleat`
--
ALTER TABLE `empleat`
ADD CONSTRAINT `empleat_idempresa_foreign` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`),
ADD CONSTRAINT `empleat_idrang_foreign` FOREIGN KEY (`idrang`) REFERENCES `rangempleat` (`id`);

--
-- Restriccions per la taula `preufinalcomanda`
--
ALTER TABLE `preufinalcomanda`
ADD CONSTRAINT `preufinalcomanda_idcomanda_foreign` FOREIGN KEY (`idcomanda`) REFERENCES `comanda` (`id`);

--
-- Restriccions per la taula `producte`
--
ALTER TABLE `producte`
ADD CONSTRAINT `producte_idtipus_foreign` FOREIGN KEY (`idtipus`) REFERENCES `tipusproducte` (`id`);

--
-- Restriccions per la taula `usersrols`
--
ALTER TABLE `usersrols`
ADD CONSTRAINT `usersrols_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Restriccions per la taula `valoracio`
--
ALTER TABLE `valoracio`
ADD CONSTRAINT `valoracio_idproducte_foreign` FOREIGN KEY (`idproducte`) REFERENCES `producte` (`id`),
ADD CONSTRAINT `valoracio_idusuari_foreign` FOREIGN KEY (`idusuari`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
