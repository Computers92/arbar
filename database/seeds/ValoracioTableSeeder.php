
<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Valoracio;

class ValoracioTableSeeder extends Seeder {

	public function run()
	{
		DB::table('valoracio')->delete();

		Valoracio::create(['idproducte' => '1', 'idusuari' => 'atorrillas',  'qualificacio' => '4', 'comentari' => 'Molt gustosa', 'llegida' => '0']);

	}

}