
<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Tipusproducte;

class TipusProducteTableSeeder extends Seeder {

public function run()
{
DB::table('tipusproducte')->delete();

	Tipusproducte::create(['nom' => 'Beguda', 'descripcio' => 'Beguda']);
	Tipusproducte::create(['nom' => 'Celiacs', 'descripcio' => 'Apte per a celiacs']);
	Tipusproducte::create(['nom' => 'Primer Plat', 'descripcio' => 'Primers plats']);
	
}

}