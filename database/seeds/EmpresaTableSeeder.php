
<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Empresa;

class EmpresaTableSeeder extends Seeder {

public function run()
{
DB::table('empresa')->delete();

	Empresa::create(['cif' => '47813422X', 'nom' => 'ArBar']);
	
}

}