<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComandasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comanda', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('idusuari')->unsigned()->index();
			$table->foreign('idusuari')->references('id')->on('users');
			
			$table->integer('idtaula')->unsigned()->index();
			$table->foreign('idtaula')->references('id')->on('taula');
			
			$table->integer('idempleat')->unsigned()->index();
			$table->foreign('idempleat')->references('id')->on('empleat');
			
			$table->boolean('estat');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comanda');
	}

}
