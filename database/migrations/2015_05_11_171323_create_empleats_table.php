<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empleat', function(Blueprint $table)
		{
			
			$table->increments('id');
			$table->integer('idempresa')->unsigned()->index();
			$table->foreign('idempresa')->references('id')->on('empresa');
			$table->integer('idrang')->unsigned()->index();
			$table->foreign('idrang')->references('id')->on('rangempleat');
			$table->string('name');
			$table->timestamps();
		});
			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empleat');
	}

}
