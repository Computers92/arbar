<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersrolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usersrols', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('iduser')->unsigned()->index();
			$table->foreign('iduser')->references('id')->on('users');
			$table->integer('idrol');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usersrols');
	}

}
