<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('producte', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('idtipus')->unsigned()->index();
			$table->foreign('idtipus')->references('id')->on('tipusproducte');
			$table->string('nom');
			$table->string('descripcio');
			$table->float('preu');
			$table->string('img');
			$table->integer('stock');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('producte');
	}

}
