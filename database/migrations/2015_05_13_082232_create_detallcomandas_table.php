<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallcomandasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detallcomanda', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('idcomanda')->unsigned()->index();
			$table->foreign('idcomanda')->references('id')->on('comanda');
			
			$table->integer('idproducte')->unsigned()->index();
			$table->foreign('idproducte')->references('id')->on('producte');
			
			$table->integer('idestat')->unsigned()->index();
			$table->foreign('idestat')->references('id')->on('estatdetallcomanda');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detallcomanda');
	}

}
