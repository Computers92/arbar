<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValoraciosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valoracio', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('idproducte')->unsigned()->index();
			$table->foreign('idproducte')->references('id')->on('producte');
			
			$table->integer('idusuari')->unsigned()->index();
			$table->foreign('idusuari')->references('id')->on('users');
			
			$table->integer('qualificacio');
			$table->string('comentari');
			$table->boolean('llegida');
			$table->timestamps();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valoracio');
	}

}
