<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreufinalcomandasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preufinalcomanda', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('idcomanda')->unsigned()->index();
			$table->foreign('idcomanda')->references('id')->on('comanda');
			
			$table->float('preu');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preufinalcomanda');
	}

}
